#include"Player.h"
#include <iostream>

Player::Player()

{
	hp = 300;
	atk = 50;
	def = 35;
}

void Player::Disphp()
{
	std::cout << "プレイヤーHP=" << hp << "\n";
}

void Player::Attack(int i)
{
	printf("プレイヤーの攻撃！");
	return atk - i / 2;
}

void Player::Damage(int i)
{
	std::cout << "プレイヤーは" << i << "のダメージ\n";
	hp -= i;
}
//防御力を取得（アクセス関数）
//引数：なし
//戻値：防御力
int Player::GetDef()
{
	return def;
}
//戦闘不能判定
//引数：なし
//戻値：戦闘不能＝true その他＝false
bool Player::IsDead()
{
	//HP が 0 以下だったら true を返す
	if (hp <= 0)
		return true;
	//それ以外なら false を返す
	return false;
}