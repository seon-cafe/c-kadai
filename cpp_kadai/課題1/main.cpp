#include"sample.h"
SampleClass* a;

int main()
{
	//クラスのインスタンス（実態）を作る
	a= new SampleClass;
	//３つのメンバ関数を呼び出す
	a->Input();
	a->Disp();
	a->Plus();
	//使い終わったインスタンスを削除
	delete a;
} 